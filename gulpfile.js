var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var uglify = require('gulp-uglify');
var elixir = require('laravel-elixir');

gulp.task('browserSync', function(){
  
  browserSync.init({
    proxy: "http://127.0.0.1:8000/"
  });

	gulp.watch('raw-assets/sass/**/*.scss', gulp.series('sass'));
    gulp.watch('**.php', gulp.series('php'));
});

gulp.task('mix', function() {
  mix.sass('raw-assets/sass/**/*.scss');
})

gulp.task('sass', function () {
  return gulp.src('raw-assets/sass/**/*.scss')
    .pipe(sass({
        outputStyle: 'expanded'
      }).on('error', sass.logError))
    .pipe(autoprefixer(['last 99 versions'], { cascade: true }))
    .pipe(gulp.dest('public/assets/css'))
    .pipe(browserSync.stream());
});

gulp.task('php', function () {
  return gulp.src('**.php')
    .pipe(browserSync.stream());
});


// gulp.task('mainjs', function () {
//   return gulp.src('assets/js/main.js')
//     .pipe(browserSync.stream());
// });

gulp.task('default', gulp.series('browserSync'));