<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultipleColumnToForms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forms', function (Blueprint $table) {
            $table->integer('stats')->length(2)->default(0);
        });
        Schema::table('blogs', function (Blueprint $table) {
            $table->char('profile', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forms', function (Blueprint $table) {
            //
        });
        Schema::table('blogs', function (Blueprint $table) {
            //
        });
        Schema::table('category', function (Blueprint $table) {
            //
        });
    }
}
