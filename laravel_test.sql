-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2020 at 03:17 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` char(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` char(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` char(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `comments` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `content`, `tags`, `author`, `profile`, `created_at`, `updated_at`, `comments`, `category`, `status`) VALUES
(174, 'tetst SFgasg', NULL, NULL, NULL, NULL, '2020-01-31 01:39:35', '2020-02-02 16:57:28', NULL, '', 0),
(175, 'Test', NULL, NULL, NULL, NULL, '2020-02-02 16:59:10', '2020-02-02 16:59:54', NULL, '', 0),
(176, 'hhhhh', NULL, NULL, NULL, NULL, '2020-02-02 17:23:19', '2020-02-02 17:23:19', NULL, '', 0),
(177, 'This is a title', NULL, NULL, NULL, NULL, '2020-02-02 17:34:41', '2020-02-02 17:35:39', NULL, '', 0),
(178, 'asgasgasg', NULL, NULL, NULL, NULL, '2020-02-02 18:06:55', '2020-02-02 18:06:55', NULL, '', 0),
(179, 'agasgasgasgasgasg agasg', NULL, NULL, NULL, NULL, '2020-02-02 18:11:13', '2020-02-02 18:11:18', NULL, '', 0),
(180, 'zzZ', '<p><br></p>\n', NULL, NULL, '', '2020-02-02 18:31:35', '2020-02-03 01:49:48', NULL, '19,18', 0),
(181, 'The new league', '<ul><li>asfasfasfasf<a href=\"https://stackoverflow.com/questions/40690381/view-errors-404-not-found\" target=\"_blank\">https://stackoverflow.com/questions/40690381/view-errors-404-not-found</a></li><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p></ul>\n', NULL, NULL, 'featured_1580720725.png', '2020-02-02 19:04:23', '2020-02-03 01:05:25', NULL, '19,18,14,13', 0),
(182, NULL, '<p><br></p>\n', NULL, NULL, '', '2020-02-03 01:32:20', '2020-02-03 01:32:20', NULL, '19,18', 0),
(183, 'Testasad', '<p>gasfasasfasfasf<p><img style=\"width: 259px;\" data-filename=\"test.jpg\" src=\"/uploads/15807810640.png\"><br></p></p>\n', NULL, NULL, 'featured_1580781064.png', '2020-02-03 01:33:43', '2020-02-03 17:51:04', NULL, '19,18', 0),
(184, 'asasgasg', '<p>asgasfasdasfasfsf</p>\n', NULL, NULL, '', '2020-02-03 01:37:10', '2020-02-03 01:39:18', NULL, '19,18', 0),
(185, 'asfasfasf', '<p><br></p>\n', NULL, NULL, '', '2020-02-03 01:42:45', '2020-02-03 01:42:45', NULL, '0', 0),
(186, 'asfasf', '<p>asasg</p>\n', NULL, NULL, '', '2020-02-03 01:51:39', '2020-02-03 17:46:31', NULL, '19,18', 0),
(187, 'qwerty', '<p><br></p>\n', NULL, NULL, '', '2020-02-03 01:52:07', '2020-02-03 01:52:07', NULL, '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` char(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `category`, `slug`) VALUES
(13, '2020-01-30 23:07:12', '2020-01-30 23:07:12', 'asdas 5', 'asdas-5'),
(14, '2020-01-30 23:08:49', '2020-01-30 23:08:49', 'tsasdasd', 'tsasdasd'),
(15, '2020-02-02 16:57:15', '2020-02-02 16:57:15', 'gasgasg', 'gasgasg'),
(16, '2020-02-02 16:59:14', '2020-02-02 16:59:14', 'gababadb', 'gababadb'),
(17, '2020-02-02 17:34:01', '2020-02-02 17:34:01', 'qweqweqwe', 'qweqweqwe'),
(18, '2020-02-02 18:07:32', '2020-02-02 18:07:32', 'bhabab', 'bhabab'),
(19, '2020-02-02 18:11:00', '2020-02-02 18:11:00', 'agasgasgasg', 'agasgasgasg');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` char(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` char(55) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `priority` char(55) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Low',
  `duedate` int(11) NOT NULL DEFAULT 1580349970,
  `assignee` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stats` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `title`, `status`, `priority`, `duedate`, `assignee`, `notes`, `created_at`, `updated_at`, `stats`) VALUES
(1, NULL, 'Pending', 'Low', 0, NULL, NULL, '2020-01-29 18:06:37', '2020-02-02 18:32:08', 0),
(3, 'gasfasfasfasf', 'Pending', 'Low', 0, NULL, NULL, '2020-01-29 18:06:37', '2020-02-02 18:32:08', 0),
(4, NULL, 'Pending', 'Low', 0, NULL, NULL, '2020-02-02 18:32:08', '2020-02-02 18:32:08', 0),
(5, NULL, 'Pending', 'Low', 0, NULL, NULL, '2020-02-02 18:32:08', '2020-02-02 18:32:08', 0),
(6, NULL, 'Pending', 'Low', 0, NULL, NULL, '2020-02-02 18:32:08', '2020-02-02 18:32:08', 0),
(7, NULL, 'Pending', 'Low', 0, NULL, NULL, '2020-02-02 18:32:08', '2020-02-02 18:32:08', 0),
(8, NULL, 'Pending', 'Low', 0, NULL, NULL, '2020-02-02 18:32:08', '2020-02-02 18:32:08', 0),
(10, NULL, 'Pending', 'Low', 0, NULL, NULL, '2020-02-02 18:32:08', '2020-02-02 18:32:08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_01_16_030825_create_cache_table', 1),
(4, '2020_01_22_074759_create_forms_table', 1),
(5, '2020_01_27_053722_add_multiple_column_to_forms', 1),
(6, '2020_01_29_084653_create_blogs_table', 1),
(7, '2020_01_30_024629_add_profile_column_to_blogs_table', 2),
(8, '2020_01_30_100038_create_blog_category_table', 2),
(9, '2020_01_31_013955_create_category_table', 2),
(10, '2020_01_31_014558_add_column_to_category_table', 3),
(11, '2020_01_31_014631_add_slug_to_category_table', 3),
(12, '2020_01_31_015129_add_category_column_to_category__table', 4),
(13, '2020_01_31_015241_add_category_column_to_category_table', 5),
(14, '2020_01_31_015427_add_category_column_to_category_table', 6),
(15, '2020_01_31_022222_add_comments_column_to_blogs_table', 7),
(16, '2020_01_31_022352_add_comments_column_to_blogs_table', 8),
(17, '2020_01_31_022557_add_category_column_to_blogs_table', 9),
(18, '2020_01_31_023005_add_category_column_to_blogs_table', 10),
(19, '2020_01_31_023115_add_category_column_to_blogs_table', 11),
(20, '2020_01_31_051733_add_status_column_to_blogs_table', 12),
(21, '2020_01_31_013955_create_categories_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Filjumar', 'jumamoy.filjumar@gmail.com', NULL, '$2y$10$00PHvv3oGJzKFD69UFj97OEdDYs6tsluHaroMkzakdfxjJisYEHPK', NULL, '2020-01-29 18:06:30', '2020-01-29 18:06:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
