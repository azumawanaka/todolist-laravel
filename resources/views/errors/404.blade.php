@extends('layouts.app')
@section('title')
    404
@endsection

@section('content')
    <section class="wrp">
        <div class="cntr">
            <div class="head-cntr dflex-ai mb-5">
                <h1 class="blog_main-title">Error @yield('title')</h1>
            </div>
        </div>
    </section>
@endsection