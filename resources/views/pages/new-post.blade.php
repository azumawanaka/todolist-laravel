@extends('layouts.app')
@section('title')
    Add New Post
@endsection

@section('content')

@if (isset($lastid) && !empty($lastid))
    <?php
        $lastid = $lastid
    ?>
@else
    <?php
        $lastid = 0
    ?>  
@endif

<input type="hidden" name="post_id" id="post_id" value="{{ $lastid }}">
    <section class="wrp">
        <div class="container">
            <div class="head-cntr dflex-ai mb-3">
                <h1 class="blog_main-title diblock">Add New Post</h1>
            </div>
            <div class="row">
                <div class="col-md-9 col-sm-7 col-xs-12">
                    <form class="form" >
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-control" id="blog-post-title" name="title" placeholder="Add Title" value="{{@$spdata[0]->title}}">
                        </div>
                        <div class="form-group">
                            <div id="summernote">
                                {!! @$spdata[0]->content !!}
                            </div>
                            <div class="placeholder">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3 col-sm-5 col-xs-12">
                    <div class="dflex-ai jc-sb mb-4">
                        <button type="submit" id="publish-post" class="btn btn-primary btn-sm" data-token="{{ csrf_token() }}">Publish</button>
                        @if (isset($lastid) && !empty($lastid))
                            <a href="{{ route('blog.show', $spdata[0]->title) }}" class="btn btn-outline-info btn-sm">Preview</a>
                        @endif
                    </div>
                    <div class="card p-3 mb-3">
                        <div class="card-body pt-0">
                            <h5 class="card-title">Categories</h5>
                            {{-- <ul class="nav nav-tabs category-list">
                                <li class="active"><a data-toggle="tab" class="btn btn-sm" href="#tab1">All Categories</a></li>
                                <li><a data-toggle="tab" class="btn btn-sm" href="#tab2">Most Used</a></li>
                            </ul>
                                 --}}
                            <div class="tab-content pt-2">
                                <div id="tab1" class="tab-pane fade in show active">
                                    <ul>
                                        @if (isset($lastid) && !empty($lastid))
                                            @if( strlen($spdata[0]->category) > 0 )
                                                @php
                                                    $cat = explode(",", $spdata[0]->category);
                                                @endphp
                                            @else 
                                                @php    
                                                    $cat = array();
                                                @endphp
                                            @endif
                                        @else
                                            @php    
                                                $cat = array();
                                            @endphp
                                        @endif
                                            @foreach ($categories as $item)
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" @if(in_array($item->id, $cat)) checked="checked" @endif class="custom-control-input category_item" id="category_{{ $item->id }}" name="category[]" value="{{ $item->id }}">
                                                        <label class="custom-control-label" for="category_{{ $item->id }}"> <small>{{ strtoupper($item->category) }}</small></label>
                                                    </div> 
                                                </li>
                                            @endforeach
                                    </ul>
                                </div>
                                {{-- <div id="tab2" class="tab-pane fade">
                                    <ul>
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="category_3" name="category[]" value="web">
                                                <label class="custom-control-label" for="category_3"> <small>Web</small></label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="category_4" name="category[]" value="foods">
                                                <label class="custom-control-label" for="category_4"> <small>Foods</small></label>
                                            </div>
                                        </li>
                                    </ul>
                                </div> --}}
                            </div>

                        </div>
                        <hr>
                        <a href="#" class="btn btn-link text-left pl-0 mb-2 pt-0" id="new_category">
                            <small><i class="fa fa-plus"></i> Add New Category</small>
                        </a>
                        <div class="new_category_field">
                            <form action="/blog/new-category" method="POST" class="form" id="save_new_category">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="">
                                        <input type="text" class="form-control" name="new_category_name" placeholder="Enter Category Name">
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button type="submit" id="token" class="btn btn-sm btn-outline-secondary" data-token="{{ csrf_token() }}">Save New Category</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card p-3">
                        <div class="card-body pt-0">
                            <h5 class="card-title">Featured Image</h5>
                            
                            @if ($featured != "")
                                <div class="img-box is-v2 mb-2">
                                    <a href="#" class="btn btn-sm text-danger" title="remove" id="editable_fimg"><i class="fa fa-remove"></i></a>
                                    <img src="/uploads/{!! $featured !!}" alt="">
                                </div>
                            @else 
                                <div class="img-box mb-2" id="featured-img-box">
                                </div>
                            @endif
                            <input type="file" id="featured-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection