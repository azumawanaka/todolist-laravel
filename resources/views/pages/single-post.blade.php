@extends('layouts.app')
@section('title')
{{$post_info[0]->title}}
@endsection

@section('content')
    <section class="wrp">
        <div class="cntr">
            <div class="head-cntr mb-5">
                <h1 class="blog_main-title">{{$post_info[0]->title}}</h1>
            </div>
            <div class="featured-img mb-4">
                <img src="/uploads/{{ $post_info[0]->profile }}" alt="" class="is-wide">
            </div>
            <div class="contents">
                {!! $post_info[0]->content !!}
            </div>
        </div>
    </section>
@endsection