@extends('layouts.app')
@section('title')
    Blog
@endsection

@section('content')
    <section class="wrp">
        <div class="container">
            @if (Auth::check())

                <div class="head-cntr dflex-ai mb-5">
                    <h1 class="blog_main-title diblock">Posts</h1> <a href="{{ route('blog') }}/new-post" class="btn btn-secondary btn-sm">Add New</a>
                </div>
                <ul class="nav nav-tabs mb-3">
                    <li class="nav-item">
                        <a class="nav-link @if (!isset($_GET['trash']) && empty($_GET['trash']))
                            active
                        @endif" href="{{ route('blog') }}"><small>({{ count($blogpost) }}) All </small></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-danger @if (isset($_GET['trash']) && !empty($_GET['trash']))
                        active
                    @endif" href="@if (count($trashed) > 0){{ route('blog') }}/?trash={{ count($trashed) }}@else javascript:; @endif"> <small>({{ count($trashed) }}) Trash</small> </a>
                    </li>
                </ul>
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="tbl-bloglist">
                        <thead>
                            <tr>
                                <th style="display:none;"></th>
                                <th scope="col">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkAll" name="checkAll" value="0"> &emsp;
                                        <label class="custom-control-label" for="checkAll"></label>
                                        Title
                                    </div>
                                </th>
                                <th scope="col">
                                    Author
                                </th>
                                <th scope="col">
                                    Categories
                                </th>
                                <th scope="col">
                                    Tags
                                </th>
                                <th scope="col">
                                    <i class="fa fa-comments"></i>
                                </th>
                                <th width="200px" scope="col">
                                    Date
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $post_loop = $blogpost;
                                $opt = "";
                            @endphp
                            @if (isset($_GET['trash']) && !empty($_GET['trash']))
                                @php
                                    $post_loop = $trashed;
                                @endphp
                            @else 
                                @php
                                    $post_loop = $blogpost;
                                @endphp
                            @endif
                            @foreach ($post_loop as $item)
                            
                                @if (isset($_GET['trash']) && !empty($_GET['trash']))
                                    @php
                                        $opt = '<ul class="list-group list-group-horizontal-sm">
                                                    <li class="list-group-item"><a href="'.route('blog').'/restore/'. $item->id .'">Restore</a></li>
                                                    <li class="list-group-item"><a href="'. route('blog') .'/destroy/'.$item->id.'" class="text-danger">Delete Permanently</a></li>
                                                </ul>';
                                    @endphp
                                @else 
                                    @php
                                        $opt = '<ul class="list-group list-group-horizontal-sm">
                                                    <li class="list-group-item"><a href="'.route('blog').'/new-post?postid='. $item->id .'">Edit</a></li>
                                                    <li class="list-group-item"><a href="'. route('blog') .'/trash/'.$item->id.'" class="text-danger">Trash</a></li>
                                                    <li class="list-group-item"><a href="'. route('blog') .'/'. $item->title .'">Preview</a></li>
                                                </ul>';
                                    @endphp
                                @endif
                                <tr class="text-danger">
                                    <td style="display:none;">
                                        {{ $item->updated_at }}
                                    </td>
                                    <td class="opt_hover">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="item_{{ $item->id }}" name="item[]" value="{{ $item->id }}">
                                            <label class="custom-control-label" for="item_{{ $item->id }}"></label>
                                            <a href="{{ route('blog') }}/{{ $item->title }}">{{ $item->title }}</a>
                                            {!! $opt !!}
                                        </div>
                                    </td>
                                    <td>
                                        @if ($item->author == '')
                                            -
                                        @else 
                                            {{ $item->author }}
                                        @endif
                                    </td>
                                    <td>
                                        <ul>
                                        @if( strlen($item->category) > 0 )
                                            @php
                                                $cat = explode(",", $item->category);
                                            @endphp
                                        @else 
                                            <li> - </li>
                                            @php    
                                                $cat = array();
                                            @endphp
                                        @endif
                                        @foreach ($category as $c_item)
                                            @if(in_array($c_item->id, $cat))
                                                <li>
                                                    <a href="#">{{ $c_item->category }}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                        </ul>
                                    </td>
                                    <td>
                                        @if ($item->tags == '')
                                            -
                                        @else 
                                            {{ $item->tags }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->comments == '')
                                            -
                                        @else 
                                            {{ $item->comments }}
                                        @endif
                                    </td>
                                    <td class="text-muted">
                                        <small>Last Modified {{ date("d M, Y H:i", strtotime($item->updated_at)) }}</small>
                                    </td>
                                </tr>
                            @endforeach
                        
                        </tbody>
                    </table>
                </div>
                
            @else 
                <div class="row">
                        @php
                            $post_loop = $blogpost;
                            $opt = "";
                        @endphp
                        
                        @if (isset($_GET['trash']) && !empty($_GET['trash']))
                            @php
                                $post_loop = $trashed;
                            @endphp
                        @else 
                            @php
                                $post_loop = $blogpost;
                            @endphp
                        @endif
                        @foreach ($post_loop as $item)
                            
                        @if (isset($_GET['trash']) && !empty($_GET['trash']))
                            @php
                                $opt = '<ul class="list-group list-group-horizontal-sm">
                                            <li class="list-group-item"><a href="'.route('blog').'/restore/'. $item->id .'">Restore</a></li>
                                            <li class="list-group-item"><a href="'. route('blog') .'/destroy/'.$item->id.'" class="text-danger">Delete Permanently</a></li>
                                        </ul>';
                            @endphp
                        @else 
                            @php
                                $opt = '<ul class="list-group list-group-horizontal-sm">
                                            <li class="list-group-item"><a href="'.route('blog').'/new-post?postid='. $item->id .'">Edit</a></li>
                                            <li class="list-group-item"><a href="'. route('blog') .'/trash/'.$item->id.'" class="text-danger">Trash</a></li>
                                            <li class="list-group-item"><a href="'. route('blog') .'/'. $item->title .'">Preview</a></li>
                                        </ul>';
                            @endphp
                        @endif
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="card">
                                <img class="card-img-top" src="/uploads/{{ $item->profile }}" alt="Card image cap">
                                <div class="card-body p-3">
                                    <h3 class="card-title">{{ $item->title }}</h3>
                                    <p class="card-text">{!! $item->content !!}</p>
                                    <a href="{{ route("blog") }}/{{$item->title}}" class="btn btn-primary mt-3 btn-block">View Post</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </section>
@endsection