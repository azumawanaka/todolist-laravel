{{-- @extends('main') --}}
@extends('layouts.app')
@section('title')
    Form
@endsection

@section('content')
    <section class="wrp">
        <div class="cntr">

            @if (Auth::check())
                {{-- form --}}
                <form action="/forms" method="POST" class="todo_form">
                    {{ csrf_field() }}
                    <div class="grp table-responsive-md">
                        <button type="button" class="btn btn-secondary btn-sm mb-2" id="add_memo"><i class="fa fa-plus"></i> Add List</button>
                        <table class="table table-sm table-striped table-dark" id="memo">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="check_all" name="check_all" value="0">
                                            <label class="custom-control-label" for="check_all"></label>
                                        </div>
                                    </th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>Priority</th>
                                    <th>Due Date</th>
                                    <th>Assignee</th>
                                    <th>Notes</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                Records: {{ count($forms) }}
                                @if (count($forms) > 0 )
                                    @foreach ($forms as $list)
                                        @if (empty($list->title))
                                            @php
                                                $empty = "border-color: red"
                                            @endphp
                                        @else
                                            @php
                                                $empty = ""
                                            @endphp
                                        @endif
                                        <tr style="{{ $empty }}">
                                            <td>
                                                <input type="hidden" name="task_id[]" value="{{ $list->id }}">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input task_item_check" name="task_item_check[]" id="check{{ $list->id }}" value="{{ $list->id }}" @if ( $list->stats == 1 )
                                                    checked
                                                @endif>
                                                    <label class="custom-control-label" for="check{{ $list->id }}"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="text" value="{{ $list->title }}" class="form-control" placeholder="Task" name="task_title[]">
                                            </td>
                                            <td>
                                                <select name="task_status[]" class="form-control" value="{{ $list->status }}">
                                                    <option value="Pending" @if( $list->status == "Pending") {{ "selected" }} @endif>Pending</option>
                                                    <option value="Done" @if( $list->status == "Done") {{ "selected" }} @endif>Done</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="task_priority[]" class="form-control">
                                                    <option value="Low" @if( $list->priority == "Low") {{ "selected='selected'" }} @endif>Low</option>
                                                    <option value="Med" @if( $list->priority == "Med") {{ "selected='selected'" }} @endif>Med</option>
                                                    <option value="High" @if( $list->priority == "High") {{ "selected='selected'" }} @endif>High</option>
                                                </select>
                                                
                                            </td>
                                            <td>
                                                <input type="date" class="form-control" placeholder="Due Date" name="task_ddate[]" value="{{ date("M, d Y", $list->duedate) }}">
                                            </td>
                                            <td><input type="text" class="form-control" placeholder="Assignee" name="task_assignee[]" value="{{ $list->assignee }}"></td>
                                            <td><textarea name="task_notes[]" class="form-control" placeholder="Notes">{{ nl2br($list->notes) }}</textarea></td>
                                            <td><a href="#" class="btn btn-danger delete-data" data-attr="{{$list->id}}" data-token="{{ csrf_token() }}"><i class="fa fa-close"></i></a></td>
                                        </tr>
                                    @endforeach
                                    
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-sm btn-primary pull-right" id="save_memo"><i class="fa fa-save"></i> Save</button>
                    </div>
                </form>
                {{-- //form --}}
            @endif   
        </div>
    </section>
@endsection