$(function(){

    let memo = $("#memo"),
        btn_memo = $("#add_memo")

        btn_memo.on("click",function(e){
            e.preventDefault()
            memo.find("tbody").prepend('<tr>'
                    +'<td><input type="hidden" name="task_id[]" value=""><input type="checkbox" name="task_item_check[]" class="task_item_check" value="1"></td>'
                    +'<td><input type="text" placeholder="Task" class="form-control" name="task_title[]"></td>'
                    +'<td><select name="task_status[]" class="form-control"><option value="Pending">Pending</option><option value="Done">Done</option></td>'
                    +'<td><select name="task_priority[]" class="form-control"><option value="Low">Low</option><option value="Medium">Med</option><option value="High">High</option></select></td>'
                    +'<td><input type="date" placeholder="Due Date" class="form-control" name="task_ddate[]"></td>'
                    +'<td><input type="text" placeholder="Assignee" class="form-control" name="task_assignee[]"></td>'
                    +'<td><textarea name="task_notes[]" class="form-control" placeholder="Notes"></textarea></td>'
                    +'<td><a href="#" class="btn btn-danger close" data-attr=""><i class="fa fa-close"></i></a></td>'
                    +'</tr>'
                    )
            return false
        })

    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });
        

    $(document).on("click",".delete-data",function(e){
        e.preventDefault()
        let id = $(this).attr("data-attr"),
            this_ = $(this)
        if(id != "") {

            if(confirm("Do you want to continue?")) {
                $.ajax({
                    type: "DELETE",
                    url: "forms/"+id,
                    data: {
                        "id": id,
                        "_token": $('meta[name="csrf-token"]').attr('content'),
                    },
                    success: function(data) {
                        this_.parents("tr").remove()
                        var data_pr = JSON.parse(data)
                        if(data_pr.success) {
                            setTimeout(function(){
                                alert(data_pr.msg)
                            }, 500)
                        }
                    },
                    error: function(data) {
                        alert('Error'+ data);
                    }
                })
            }else{
                return false
            }
            
        }else{
            this_.parents("tr").remove()
        }

        return false
    })

    $(document).on("click","#check_all",function(e){ 
        
    })

    $(document).on("click","#new_category",function(e){
        e.preventDefault()
        $(this).next().fadeToggle(500)
        return false
    })

    $(document).on("submit","#save_new_category",function(e){
        e.preventDefault()

        $("#alert-cntr").find(".alert").remove()

        var nc = $(this).find("[name=new_category_name]").val(),
            token = $("#token").attr("data-token")

        if(nc == "") {
            $(this).find("[name=new_category_name]").focus()
            $("#alert-cntr").append('<div class="alert alert-warning" alert-block fade show">'
                    +'<button type="button" class="close" data-dismiss="alert">×</button>'
                    +'<strong>Warning!</strong> Empty field! Please add category name.</div>')
            return false
        }else{
            
            $.ajax({
                type: "POST",
                url: "/blogs/new-category",
                data: { 
                    "cat": nc,
                    "_token": token,
                },
                success: function(data) {
                    var data_parse = JSON.parse(data),
                        err = "danger",
                        err_strong = "Success!"
                    if(data_parse.success) {
                        err_strong = "Success!"
                        err = "success"
                        $("#tab1")
                        .find("ul")
                        .prepend(
                                '<li>'
                                    +'<div class="custom-control custom-checkbox">'
                                            +'<input type="checkbox" class="custom-control-input" id="category_1" name="category[]" value="'+nc+'" checked="checked">'
                                            +'<label class="custom-control-label" for="category_1"> <small>'+nc+'</small></label>'
                                        +'</div>'
                                +'</li>'
                                )  
                    }else{
                        err_strong = "Error!"
                        err = "danger"
                        $("[name=new_category_name]").focus()
                    }

                    $("#alert-cntr").append('<div class="alert alert-'+err+' alert-block fade-in show">'
                    +'<button type="button" class="close" data-dismiss="alert">×</button>'
                    +'<strong>'+err_strong+'</strong> '+data_parse.msg+'</div>')

                }
            })
 
        }


    })

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader()
            reader.onload = function (e) {
                $('#featured-img-box').html("<img src='"+e.target.result+"'>")
                // $('#featured-img-box').css({
                //     backgroundImage: 'url('+e.target.result+')',
                //     backgroundPosition: 'center center',
                //     backgroundSize: 'cover'
                // })
            }
            reader.readAsDataURL(input.files[0])
        }
    }
    $(document).on("click","#featured-img-box",function(){
        $("#featured-img").trigger("click")
    })

    $("#featured-img").change(function(){
        readURL(this)
    });

    $(document).on("click","#publish-post",function(e){
        e.preventDefault()
        $("#alert-cntr").find(".alert").remove()

        var post_title = $('#blog-post-title').val(),
            last_id = $("#post_id").val(),
            contents = $(".note-editable").html(),
            featured = $("#featured-img-box img").attr("src"),
            token = $("#token").attr("data-token")

        var check_cat = [];
            $('.category_item:checked').each(function(i){
                check_cat[i] = $(this).val();
            });

        if(post_title == "") {
            $("#alert-cntr").append('<div class="alert alert-warning" alert-block show">'
                    +'<button type="button" class="close" data-dismiss="alert">×</button>'
                    +'<strong>Warning!</strong> Empty Title. Please add Title.</div>')

            return false
        }else{
            $.ajax({
                type: "POST",
                url: "/blog/new-post",
                data: { 
                    "title": post_title,
                    "id": last_id,
                    "cat": check_cat,
                    "cont": contents,
                    "featured" : featured,
                    "_token": token, 
                },
                success: function(data) {
                    var post_data = JSON.parse(data)

                    $("#post_id").val(post_data.last_insert_id)

                    $("#alert-cntr").append('<div class="alert alert-success" alert-block show">'
                    +'<button type="button" class="close" data-dismiss="alert">×</button>'
                    +'<strong>Success!</strong> Data successfully saved. Refreshing page..</div>')

                    setTimeout(function(){
                        window.location.href = "?postid="+post_data.last_insert_id
                    }, 2000)
                }

            })
        }

        return false

    })

    $(document).on("click","#editable_fimg",function(e){
        e.preventDefault()
        $(this).parents(".card-body").append('<div class="img-box mb-2" id="featured-img-box"></div>')
        $(this).parents(".img-box.is-v2").remove()
        return false
    })

})