<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Form;
class Form extends Model
{
    protected $fillable = ['title','status','priority','duedate','assignee','notes'];
}
