<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Session;
use DB;
use App\Form;

class FormsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $form = \DB::table('forms')->select()->orderBy('id', "desc")->get();
        return view('pages/form', ['forms'=>$form]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('task_id');
        $task_title = $request->input('task_title');
        $task_status = $request->input('task_status');
        $task_priority = $request->input('task_priority');
        $task_ddate = $request->input('task_ddate');
        $task_assignee = $request->input('task_assignee');
        $task_notes = $request->input('task_notes');
        // // $task_done = $request->input('task_item_check');

        $msg = array();
        
        for($i = 0; $i < count($id); $i++) {
            $forms = new Form;
            if(empty($id[$i]))
            {
                Form::create([
                    'title' => $request->input('task_title')[$i],
                    'status' => $request->input('task_status')[$i],
                    'priority' => $request->input('task_priority')[$i],
                    'duedate' => strtotime($request->input('task_ddate')[$i]),
                    'assignee' => $request->input('task_assignee')[$i],
                    'notes' => $request->input('task_notes')[$i]
                ]);

                $msg = array("type" => "success", "title" => "Success!", "msg" => " Data was successfully saved.");

            }else{
                Form::find($id[$i])->update(
                    [
                        "title" => $request->input('task_title')[$i],
                        "status" => $request->input('task_status')[$i],
                        "priority" => $request->input('task_priority')[$i],
                        "duedate" => strtotime($request->input('task_ddate')[$i]),
                        "assignee" => $request->input('task_assignee')[$i],
                        "notes" => $request->input('task_notes')[$i],
                    ]
                );

                $msg = array("type" => "success", "title" => "Success!", "msg" => " Data was successfully updated.");
            }
        }


        return \Redirect::to('todo')->with('message', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = Form::find($id);
    
        if($form->delete($id)) {
            return json_encode(array(
                'success' => true,
                'msg'   => 'Data successfully deleted!'
            ));
        }
        
    }

    public function get_reward_level(Request $request) {
        $input_1 = $request->input("money1");
        $input_2 = $request->input("money2");
        $total = $input_1 + $input_2;

        $reward = "";
        if( $total > 0 && $total < 125 ) {
            $reward = "Level: White Member";
        }else if( $total > 124 && $total < 1000 ) {
            $reward = "Level: Blue Member";  
        }else if( $total > 999 && $total < 1999 ) {
            $reward = "Level: Silver Member"; 
        }else if( $total > 1999 ) {
            $reward = "Level: Gold Member"; 
        }

        echo "Input 1: $".$input_1." Input 2: $".$input_2." ".$reward;

    }
}
