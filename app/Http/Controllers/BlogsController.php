<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Blogs;
use App\Categories;
use App\ Summernote;


class BlogsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = \DB::table('blogs')->where("status", 0)->select()->orderBy('id', "desc")->get();
        $trashed = \DB::table('blogs')->where("status", 1)->select()->orderBy('id', "desc")->get();
        $cat = \DB::table('categories')->select()->orderBy('id', "desc")->get();
        
        return view('pages/blog', ["blogpost" => $blog, "trashed" => $trashed, "category" => $cat]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      
    }

    public function saveCategory(Request $request) {
        $cat = strtolower($request->input('cat'));

        $msg = array();

        if (Categories::where('category', '=', $cat)->exists()) {
            $msg = array(
                "success" => false,
                "msg"   => "Category already exist!"
            );
        }else{
        
            $update_create = Categories::updateOrCreate(
                [
                "category"  =>  $cat,
                "slug"  => preg_replace('/\s+/', '-', strtolower($cat))
                ]);
            if($update_create) {
                $msg = array(
                    "success" => true,
                    "msg"   => "Data was successfully added!"
                );
            }else{
                $msg = array(
                    "success" => false,
                    "msg"   => "An error occured"
                );
            }
        }

        echo json_encode($msg);
    }

    public function savePost(Request $request) {
        
        $id = $request->input("id");
        $title = $request->input('title');
        $category = $request->input('cat');
        $contents = $request->input('cont');
        $featured_img = $request->input('featured');

        if(isset($featured_img) && !empty($featured_img)) {
            $f_img = $featured_img;

            list($type, $f_img) = explode(';', $f_img);
            list(, $f_img)      = explode(',', $f_img);

            $f_img = base64_decode($f_img);
            $fimg_name = "featured_".time().'.png';
            $path = public_path() .'/uploads/'. $fimg_name;

            file_put_contents($path, $f_img);
        }else{
            $fimg_name = "";
        }

        $detail = $contents;

		$dom = new \domdocument();
		$dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');
        
		foreach($images as $k => $img){
			$data = $img->getattribute('src');

			list($type, $data) = explode(';', $data);
			list(, $data)      = explode(',', $data);

			$data = base64_decode($data);
			$image_name= time().$k.'.png';
			$path = public_path() .'/uploads/'. $image_name;
			file_put_contents($path, $data);
			$img->removeattribute('src');
			$img->setattribute('src', '/uploads/'.$image_name);
		}

		$detail = $dom->savehtml();

        if( isset($category) && !empty($category) ){
            $new_cat = implode(',', $category);
        }else{
            $new_cat = 0;
        }

        if (Blogs::where('id', '=', $id)->exists()) {
            
            $data = Blogs::find($id)->update(
            [
                "title"  =>  $title,
                "category"  => $new_cat,
                "content"   => $detail,
                "profile"   => $fimg_name,
                "status"    => 0
            ]);
        
            return json_encode(array('success' => true, 'last_insert_id' => $id, 'summernote' => $detail), 200);

        }else{

            $data = Blogs::create(
            [
                "title"  =>  $title,
                "category"  => $new_cat,
                "content"   => $detail,
                "profile"   => $fimg_name
            ]);
        
            return json_encode(array('success' => true, 'last_insert_id' => $data->id, 'summernote' => $detail), 200);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
        $post = Blogs::where('title',$title)->get();
        
        if(count($post)>0) {
            return \View::make('pages/single-post', ['post_info'=>$post])->with('blog', $post);
        }else{
            return \Response::view('errors.404',array(),404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function trash($id) {
        
        $data = Blogs::find($id)->update(
        [
            "status"  =>  1
        ]);

        if($data) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => " Data was successfully moved to trash.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => " Something went wrong. Please try again later.");
        }
        
        return \Redirect::to('blog')->with('message', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blogs::find($id);
        
        if($blog->delete($id)) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => " Data was successfully deleted.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => " Something went wrong. Please try again later.");
        }
        
        return \Redirect::to('blog')->with('message', $msg);
    }

    public function restore($id) {

        $data = Blogs::find($id)->update(
        [
            "status"  =>  0
        ]);

        if($data) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => " Data was successfully moved to restored.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => " Something went wrong. Please try again later.");
        }
        
        return \Redirect::to('blog')->with('message', $msg);
    }

    public function new(Request $request) {
        $last_id = $request->input("postid");

        $singlePostData = \DB::table('blogs')->select()->where("id", $last_id)->get();

        $title = "";
        $category = "";
        $profile = "";

        $cat = \DB::table('categories')->select()->orderBy('id', "desc")->get();

        if(count($singlePostData) > 0) {
            $title = $singlePostData[0]->title;
            $category = $singlePostData[0]->category;
            $profile = $singlePostData[0]->profile;

            
            return view('pages/new-post', [
                'categories'    =>  $cat,
                'lastid'    => $last_id,
                'spdata'    => $singlePostData,
                'post_title'    => $title,
                'category'  => $category,
                'featured'  => $profile
            ]);
        }else{
            
            return view('pages/new-post', [
                'categories'    =>  $cat,
                'lastid'    => $last_id,
                'spdata'    => $singlePostData,
                'post_title'  => "",
                'category'  => "",
                'featured'  => $profile

            ]);
        }
        

    }
}
